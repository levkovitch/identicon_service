# cmake

cmake_minimum_required(VERSION 3.12)

project(identicon_service)

include(cmake/build.cmake)

add_subdirectory(third-party)

find_package(OpenCV COMPONENTS core imgproc imgcodecs REQUIRED)
find_package(Boost  COMPONENTS program_options stacktrace_backtrace REQUIRED)
find_package(OpenSSL REQUIRED)
find_package(Threads REQUIRED)


set(PROGRAM_SOURCES
  src/main.cpp
  src/stats_service.cpp
  src/identicon_service.cpp
  )

add_executable(${PROJECT_NAME} ${PROGRAM_SOURCES})
target_compile_features(${PROJECT_NAME} PRIVATE cxx_std_17)
target_include_directories(${PROJECT_NAME} PRIVATE
  ${OpenCV_INCLUDE_DIRS}
  src
  )
target_link_libraries(${PROJECT_NAME} PRIVATE
  restpp
  ${OpenCV_LIBS}
  Boost::boost
  OpenSSL::SSL
  ${Boost_LIBRARIES}
  Threads::Threads
  dl
  )
