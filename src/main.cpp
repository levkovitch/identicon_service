// main.cpp

#include "identicon_service.hpp"
#include "stats_service.hpp"
#include <boost/asio/io_context.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/stacktrace.hpp>
#include <condition_variable>
#include <csignal>
#include <cstdint>
#include <cstdlib>
#include <restpp/http.hpp>
#include <restpp/server.hpp>
#include <restpp/service.hpp>
#include <simple_logs/logs.hpp>


#define HELP_FLAG         "help"
#define URL_FLAG          "url"
#define THREAD_COUNT_FLAG "threads"
#define MAX_SESSION_COUNT "max_session_count"
#define VERBOSE_FLAG      "verbose"
#define VERBOSE_SHORT     "v"

#define DEFAULT_HOST              "localhost"
#define DEFAULT_PORT              8000
#define DEFAULT_THREAD_COUNT      1
#define DEFAULT_MAX_SESSION_COUNT 0 // not limited


void sigsegv_handler([[maybe_unused]] int signal) {
  // XXX note that application must be compiled with -g flag
  boost::stacktrace::stacktrace trace{};
  LOG_FAILURE(boost::stacktrace::to_string(trace));
}


namespace http = restpp::http;
namespace asio = boost::asio;


int main(int argc, char *argv[]) {
  std::signal(SIGSEGV, sigsegv_handler);
  std::signal(SIGABRT, sigsegv_handler);


  // describe program options
  namespace po = boost::program_options;
  po::options_description desc{"provide rest api for users managment"};
  // clang-format off
  desc.add_options()
    (HELP_FLAG, "print description")
    (URL_FLAG, po::value<std::string>()->default_value("http://" DEFAULT_HOST ":" + std::to_string(DEFAULT_PORT)), "url for the service")
    (THREAD_COUNT_FLAG, po::value<ushort>()->default_value(DEFAULT_THREAD_COUNT), "count of started threads")
    (MAX_SESSION_COUNT, po::value<size_t>()->default_value(DEFAULT_MAX_SESSION_COUNT), "maximum count of listened connections by the server")
    (VERBOSE_FLAG "," VERBOSE_SHORT, "show more logs");
  // clang-format on

  // parse arguments
  po::variables_map args;
  po::store(po::parse_command_line(argc, argv, desc), args);

  if (args.count(HELP_FLAG) != 0) {
    std::cout << desc << std::endl;
    return EXIT_FAILURE;
  }

  po::notify(args);


  // setup simple logs
  auto infoBackend  = std::make_shared<logs::TextStreamBackend>(std::cout);
  auto infoFrontend = std::make_shared<logs::LightFrontend>();
  infoFrontend->setFilter(logs::Severity::Placeholder == logs::Severity::Info);

  auto errorBackend  = std::make_shared<logs::TextStreamBackend>(std::cerr);
  auto errorFrontend = std::make_shared<logs::LightFrontend>();
  errorFrontend->setFilter(logs::Severity::Placeholder >=
                           logs::Severity::Error);

  LOGGER_ADD_SINK(infoFrontend, infoBackend);
  LOGGER_ADD_SINK(errorFrontend, errorBackend);

  if (args.count(VERBOSE_FLAG)) {
    auto debugBackend  = std::make_shared<logs::TextStreamBackend>(std::cerr);
    auto debugFrontend = std::make_shared<logs::LightFrontend>();
    debugFrontend->setFilter(
        logs::Severity::Placeholder == logs::Severity::Trace ||
        logs::Severity::Placeholder == logs::Severity::Debug ||
        logs::Severity::Placeholder == logs::Severity::Warning ||
        logs::Severity::Placeholder == logs::Severity::Throw);

    LOGGER_ADD_SINK(debugFrontend, debugBackend);
  }


  std::string server_uri        = args[URL_FLAG].as<std::string>();
  ushort      thread_count      = args[THREAD_COUNT_FLAG].as<ushort>();
  size_t      max_session_count = args[MAX_SESSION_COUNT].as<size_t>();

  LOG_INFO("server uri:             %1%", server_uri);
  LOG_INFO("count of threads:       %1%", thread_count);
  LOG_INFO("maximum session count:  %1%", max_session_count);


  asio::io_context io_context;

  restpp::server_builder server_builder{io_context};
  server_builder.set_max_session_count(max_session_count);
  server_builder.set_uri(server_uri);
  server_builder.add_service(std::make_shared<identicon_service_factory>(),
                             "/identicon");
  server_builder.add_service(std::make_shared<stats_service_factory>(),
                             "/stats");
  server_builder.set_req_body_limit(1024 * 1000 * 8);

  // start the server
  LOG_INFO("start server");
  restpp::server server = server_builder.build();
  server.async_run();


  asio::signal_set sigset{io_context, SIGINT, SIGTERM};
  sigset.async_wait([&io_context, &server](boost::system::error_code e, int) {
    if (e.failed()) {
      LOG_ERROR(e.message());
    }

    server.stop();
    io_context.stop();
  });


  std::vector<std::thread> threads;
  for (int i = 0; i < thread_count - 1; ++i) {
    threads.emplace_back([&io_context]() {
      io_context.run();
    });
  }

  io_context.run();
  LOG_INFO("exit");

  for (std::thread &thread : threads) {
    thread.join();
  }

  return EXIT_SUCCESS;
}
