// identicon_service.cpp

#include "stats_service.hpp"
#include <identicon_service.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <openssl/md5.h>
#include <regex>
#include <restpp/http.hpp>
#include <simple_logs/logs.hpp>


#define MIME_JPEG      "image/jpeg"
#define MIME_JPG       "image/jpg"
#define MIME_PNG       "image/png"
#define MIME_ANY_IMAGE "image/*"
#define MIME_ANY       "*/*"
#define MIME_DEFAULT   MIME_JPEG


#define ARG_WIDTH    "width"
#define ARG_HEIGHT   "width"
#define ARG_IDENTITY "width"


#define MIN_IMAGE_WIDTH  12
#define MIN_IMAGE_HEIGHT 12
#define MAX_IMAGE_WIDTH  512
#define MAX_IMAGE_HEIGHT 512


#define IDENTICON_TAILS_ON_SIDE 4


namespace http = restpp::http;
using namespace http::literals;


cv::Mat     create_identicon(size_t width, size_t height, std::string identity);
std::string serialize_image(cv::Mat img, std::string mime);


void identicon_service::handle(http::request &req, http::response &res) {
  stats_service::increment(req);


  if (req.method() != http::verb::get) {
    LOG_WARNING("method not allowed: %1%", req.method_string());
    res.result(http::status::method_not_allowed);
    return;
  }


  http::url::path path{http::url::get_path(req.relative())};
  if (path != "/"_path) {
    LOG_WARNING("not found: %1%", path);
    res.result(http::status::not_found);
    return;
  }

  this->make_identicon(req, res);
}

void identicon_service::make_identicon(http::request & req,
                                       http::response &res) {
  // check params
  std::string output_mime_type{req[http::header::accept]};
  if (output_mime_type == MIME_ANY || output_mime_type == MIME_ANY_IMAGE) {
    output_mime_type = MIME_DEFAULT;
  }
  if (output_mime_type != MIME_JPG && output_mime_type != MIME_JPEG &&
      output_mime_type != MIME_PNG) {
    LOG_WARNING("unsuported accept type: %1%", output_mime_type);
    res.result(http::status::not_acceptable);
    return;
  }


  std::string_view query_str  = http::url::get_query(req.target());
  http::url::args  query_args = http::url::query::split(query_str);
  if (query_args.count(ARG_WIDTH) == 0 || query_args.count(ARG_HEIGHT) == 0 ||
      query_args.count(ARG_IDENTITY) == 0) {
    LOG_WARNING("not found args: %1%/%2%/%3%",
                ARG_WIDTH,
                ARG_HEIGHT,
                ARG_IDENTITY);
    res.result(http::status::precondition_failed);
    res.body() = "not found args: " ARG_WIDTH "/" ARG_HEIGHT "/" ARG_IDENTITY;
    return;
  }

  int         width    = 0;
  int         height   = 0;
  std::string identity = "";
  try {
    width    = stoi(query_args.at(ARG_WIDTH));
    height   = stoi(query_args.at(ARG_HEIGHT));
    identity = query_args.at(ARG_IDENTITY);
  } catch (std::exception &e) {
    LOG_WARNING("invalid args: %1%/%2%/%3%",
                ARG_WIDTH,
                ARG_HEIGHT,
                ARG_IDENTITY);
    res.result(http::status::precondition_failed);
    res.body() = "invalid args: " ARG_WIDTH "/" ARG_HEIGHT "/" ARG_IDENTITY;
  }

  if (identity.empty() || width < MIN_IMAGE_WIDTH || width > MAX_IMAGE_WIDTH ||
      height < MIN_IMAGE_HEIGHT || height > MAX_IMAGE_HEIGHT) {
    LOG_WARNING("invalid args: %1%/%2%/%3%",
                ARG_WIDTH,
                ARG_HEIGHT,
                ARG_IDENTITY);
    res.result(http::status::precondition_failed);
    res.body() = "invalid args: " ARG_WIDTH "/" ARG_HEIGHT "/" ARG_IDENTITY;
  }


  // create identicon
  cv::Mat     identicon = create_identicon(width, height, identity);
  std::string buffer    = serialize_image(identicon, output_mime_type);


  // response
  res.set(http::header::content_type, output_mime_type);
  res.body() = buffer;
}


cv::Mat create_identicon(size_t width, size_t height, std::string identity) {
  std::vector<uchar> md5sum(MD5_DIGEST_LENGTH);
  ::MD5(reinterpret_cast<const unsigned char *>(identity.data()),
        identity.size(),
        md5sum.data());

  cv::Mat identicon{cv::Size{IDENTICON_TAILS_ON_SIDE, IDENTICON_TAILS_ON_SIDE},
                    CV_8UC3,
                    cv::Scalar{255, 255, 255}};

  // TODO not very well algo: we use only for values from md5, think about it
  cv::Vec3b color{md5sum[0], md5sum[1], md5sum[2]};
  uchar     fillFlags   = md5sum[3];
  uchar     currentFlag = 1;


  // here we iterate left part of image and filled it by fillFlags
  for (int col = 0; col < IDENTICON_TAILS_ON_SIDE; ++col) {
    for (int row = 0; row < IDENTICON_TAILS_ON_SIDE / 2; ++row) {
      if (fillFlags & currentFlag) {
        identicon.at<cv::Vec3b>(cv::Point{row, col}) = color;
      }
      currentFlag <<= 1;
    }
  }

  // also we copy left part of identicon to right by flipping
  cv::flip(identicon(cv::Rect{
               cv::Point{0, 0},
               cv::Size{IDENTICON_TAILS_ON_SIDE / 2, IDENTICON_TAILS_ON_SIDE}}),
           identicon(cv::Rect{
               cv::Point{IDENTICON_TAILS_ON_SIDE / 2, 0},
               cv::Point{IDENTICON_TAILS_ON_SIDE, IDENTICON_TAILS_ON_SIDE}}),
           1);

  cv::Mat retval;
  cv::resize(identicon,
             retval,
             cv::Size(width, height),
             0,
             0,
             cv::INTER_NEAREST);

  return retval;
}

std::string serialize_image(cv::Mat img, std::string mime) {
  const std::regex mime_reg{R"(image/(\w+))"};

  std::string type;
  if (std::smatch match;
      std::regex_match(mime, match, mime_reg) && match.size() == 2) {
    type = match[1];
  } else {
    LOG_THROW(std::invalid_argument, "invalid mime");
  }


  std::vector<uchar> serialized;
  bool               ok = cv::imencode("." + type, img, serialized);
  if (ok == false) {
    LOG_THROW(std::runtime_error, "can't encode image");
  }


  std::string retval;
  std::copy(serialized.begin(), serialized.end(), std::back_inserter(retval));
  return retval;
}
