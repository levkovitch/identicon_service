// identicon_service.hpp

#pragma once

#include <restpp/service.hpp>

class identicon_service final : public restpp::service {
public:
  void handle(restpp::http::request &req, restpp::http::response &res) override;

  void make_identicon(restpp::http::request &req, restpp::http::response &res);
};

class identicon_service_factory final : public restpp::service_factory {
public:
  restpp::service_ptr make_service() override {
    return std::make_unique<identicon_service>();
  }
};
