// asserts.hpp

#pragma once

#define ASSERT(ok, msg)                                                        \
  if ((ok) == false) {                                                         \
    LOG_THROW(std::invalid_argument, msg);                                     \
  }
