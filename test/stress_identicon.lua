-- stress_identicon.lua
--
local wrk = _G.wrk

local endpoint = "/identicon"

local width = 128
local height = 128

local output_mime = "image/jpeg"

local indentities = {"alpha", "bravo", "charlie", "delta", "echo", "foxtrot",
                     "india", "juliet", "lima", "mike", "novemver"}

function _G.request()
  local num = math.random(1, #indentities)
  local identity = indentities[num]
  return wrk.format("GET",
                    string.format("%s?width=%d&height=%d&identity=%s", endpoint,
                                  width, height, identity),
                    {["Accept"] = output_mime})
end

function _G.response(status, _, body)
  if status ~= 200 then
    print(string.format("invalid response: %d %s", status, body or ""))
  end
end
